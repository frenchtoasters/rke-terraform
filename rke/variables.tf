variable "master_addresses" {
  type = "list"
  default = []
}

variable "worker_addresses" {
  type = "list"
  default = []
}

variable "aio_addresses" {
  type = "list"
  default = []
}

variable "rancher_ssh_key" {
  type = "string"
  description = "ssh_key to access nodes"
}

variable "edge_ip" {
  type = "string"
  description = "External IP of the edge"
}

variable "rancher_config_template" {
  type = "string"
  description = "cloud config template to use"
  default = "nodelist.json.tmpl"
}

variable "cluster_domain" {
  type = "string"
}