locals {
  api_nodes = concat(var.master_addresses, var.aio_addresses)
  worker_nodes = concat(var.worker_addresses, var.aio_addresses)
}

data "aws_route53_zone" "base" {
  name = var.base_domain
  private_zone = true
}

resource "aws_route53_zone" "cluster" {
  name          = var.cluster_domain
  force_destroy = true

  tags = map(
    "Name", var.cluster_domain,
	  "Platform", "vCD"
    )
}

/*
resource "aws_route53_record" "name_server" {
  name    = var.cluster_domain
  type    = "NS"
  ttl     = "300"
  zone_id = data.aws_route53_zone.base.zone_id
  records = aws_route53_zone.cluster.name_servers
}
*/

resource "aws_route53_record" "api-external" {
  type           = "A"
  ttl            = "60"
  zone_id        = aws_route53_zone.cluster.zone_id
  name           = "api.${var.cluster_domain}"
  set_identifier = "api"
  records        = local.api_nodes

  weighted_routing_policy {
    weight = 90
  }
}

resource "aws_route53_record" "api-internal" {
  type           = "A"
  ttl            = "60"
  zone_id        = aws_route53_zone.cluster.zone_id
  name           = "api-int.${var.cluster_domain}"
  set_identifier = "api"
  records        = local.api_nodes

  weighted_routing_policy {
    weight = 90
  }
}

resource "aws_route53_record" "etcd_a_nodes" {
  count   = length(local.api_nodes)

  type    = "A"
  ttl     = "60"
  zone_id = aws_route53_zone.cluster.zone_id
  name    = "etcd-${count.index}.${var.cluster_domain}"
  records = ["${element(local.api_nodes, count.index)}"]
}

resource "aws_route53_record" "etcd_cluster" {
  type    = "SRV"
  ttl     = "60"
  zone_id = aws_route53_zone.cluster.zone_id
  name    = "_etcd-server-ssl._tcp"
  records = formatlist("0 10 2380 %s", aws_route53_record.etcd_a_nodes.*.fqdn)
}

resource "aws_route53_record" "ingress" {
  type    = "A"
  ttl     = "60"
  zone_id = aws_route53_zone.cluster.zone_id
  name    = "*.apps.${var.cluster_domain}"
  records = local.worker_nodes
}

resource "aws_route53_record" "compute_nodes" {
  count   = length(local.worker_nodes)

  type    = "A"
  ttl     = "60"
  zone_id = aws_route53_zone.cluster.zone_id
  name    = "compute-${count.index}.${var.cluster_domain}"
  records = [element(local.worker_nodes, count.index)]
}
