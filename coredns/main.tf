data "template_file" "corefile" {
  template = "${file("${path.module}/corefile.tmpl")}"

  vars = {
    cluster_domain = var.cluster_domain
    zone_id = var.zone_id
    access_key = var.access_key
    secret_key = var.secret_key
  }
}

resource "null_resource" "corefile" {
  triggers = {
    template_id = data.template_file.corefile.id
  }

  connection {
    type = "ssh"
    user = "root"
    private_key = file("~/.ssh/id_rsa")
    host = var.edge_ip
  } 
  provisioner "file" {
    content = data.template_file.corefile.rendered
    destination = "/etc/coredns/Corefile"
  }  

  depends_on = ["bastion_host"]
}

resource "null_resource" "restart_coredns" {
  triggers = {
    template_id = data.template_file.corefile.id
  }

  connection {
    type = "ssh"
    user = "root"
    private_key = file("~/.ssh/id_rsa")
    host = var.edge_ip
  }

  provisioner "remote-exec" {
    inline = [
      "systemctl restart coredns"
    ]
  }

  depends_on = ["bastion_host"]
}