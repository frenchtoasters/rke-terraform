output "master_nodes" {
 value = vcd_vapp_vm.master-vm[*].network[0].ip
}

output "worker_nodes" {
  value = vcd_vapp_vm.worker-vm[*].network[0].ip
}

output "aio_nodes" {
  value = vcd_vapp_vm.aio-vm[*].network[0].ip
}

output "bastion_host" {
  value = vcd_vapp_vm.bastion-vm[*].id
}