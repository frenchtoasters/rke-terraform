locals {
  master_nodes = length(var.master_addresses) > 0 ? yamldecode(templatefile("${path.module}/${var.rancher_config_template}", {ip_addresses = "${var.master_addresses}"})) : []
  worker_nodes = length(var.worker_addresses) > 0 ? yamldecode(templatefile("${path.module}/${var.rancher_config_template}", {ip_addresses = "${var.worker_addresses}"})) : []
  aio_nodes = length(var.aio_addresses) > 0 ? yamldecode(templatefile("${path.module}/${var.rancher_config_template}", {ip_addresses = "${var.aio_addresses}"})) : []
}

resource "rke_cluster" "cluster" {
  bastion_host {
    address = var.edge_ip
    user = "root"
    port = "22"
    ssh_key_path = "~/.ssh/id_rsa"
  }

  dynamic nodes {
    for_each = local.master_nodes
    content {
      node_name = nodes.value.name
      address = nodes.value.address
      user = nodes.value.user
      role = ["controlplane", "etcd"]
      ssh_key_path = var.rancher_ssh_key
      labels = {
        cluster = "${var.cluster_domain}"
        aio = "false"
        master = "true"
      }
    }
  }

  dynamic nodes {
    for_each = local.worker_nodes
    content {
      node_name = nodes.value.name
      address = nodes.value.address
      user = nodes.value.user
      role = ["worker"]
      ssh_key_path = var.rancher_ssh_key
      labels = {
        cluster = "${var.cluster_domain}"
        aio = "false"
        master = "false"
      }
    }
  }

  dynamic nodes {
    for_each = local.aio_nodes
    content {
      node_name = nodes.value.name
      address = nodes.value.address
      user = nodes.value.user
      role = ["controlplane", "etcd", "worker"]
      ssh_key_path = var.rancher_ssh_key
      labels = {
        cluster = "${var.cluster_domain}"
        aio = "true"
        master = "true"
      }
    }
  }
  
  services_etcd {
    backup_config {
      interval_hours = "6"
      retention = "24"
    }
  }
}

resource "local_file" "kube_cluster_yaml" {
  filename = format("%s/%s" , path.root, "kube_config_cluster.yml")
  content = rke_cluster.cluster.kube_config_yaml
}

resource "local_file" "rke_cluster_yaml" {
  filename = format("%s/%s", path.root, "cluster.yml")
  content = rke_cluster.cluster.rke_cluster_yaml
}

resource "local_file" "rke_state" {
  filename = format("%s/%s", path.root, "cluster.rkestate")
  content = rke_cluster.cluster.rke_state
}