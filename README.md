# RKE Terraform

This repository contains the terraform needed to deploy a multi node RKE cluster. 

## Bastion Host Requirements

* You need to ensure that the bastion host can be configured with the specificed IP address via the `guestinfo` parameter.

* The `~/.ssh/id_rsa.pub` key _of the host running terrafom_, needs to be added to the `authorized_ssh_keys` of the bastion host template.

## Notes

* Currently all `roles` are applied each of the nodes that are deployed. 


### Template requirements

* The `template_name` variable is the one used for the cluster nodes. This template is assumed to be a version of `ros` or some operating system that can be configured via a `cloud_init` file and `guestinfo` parameters. 