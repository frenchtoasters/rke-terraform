//////
// vCD variables
//////

variable "vcd_url" {
  type        = string
  description = "This is the url for the vCD environment."
}

variable "vcd_user" {
  type        = string
  description = "vCD server user for the environment."
}

variable "vcd_password" {
  type        = string
  description = "vCD server password"
}

variable "vcd_org" {
  type        = string
  description = "This is the name of the vCD org."
}

variable "vcd_vdc" {
  type        = string
  description = "This is the name of the vCD Virtual Data Center."
}

variable "catalog_name" {
  type        = string
  description = "This is the name of the catalog."
}

variable "template_name" {
  type        = string
  description = "This is the name of the VAPP template to clone."
}

variable "vapp_network" {
  type        = string
  description = "This is the name of the publicly accessible network for cluster ingress and access."
  default     = "VM Network"
}

variable "vcd_edge" {
  type        = string
  description = "The vCD edge name to use for deployment."
  default     = ""
}

variable "vcd_static_ip_start" {
  type = string
  description = "The start of the static ip pool range"
}

variable "vcd_static_ip_end" {
  type = string
  description = "The end of the static ip pool range"
}

variable "vcd_external_network" {
  type = string
  description = "Name of the provider external network"
}

variable "vcd_network_gateway" {
  type = string
  description = "The gateway for the vcd network"
}

variable "vcd_network_cidr" {
  type = "string"
  description = "The CIDR of the network"
}

variable "cluster_domain" {
  type = string
  description = "Domain used for cluster"
}

variable "base_domain" {
  type = string
  description = "Base domain for cluster"
}

/*
variable "vcd_lb_name" {
  type        = string
  description = "The vCD loadbalancer name to use for deployment."
  default     = ""
}
*/

variable "vcd_edge_ip" {
  type        = string
  description = "The external IP of the NSX Edge."
  default     = ""
}

/////////
// AWS variables
////////

variable "aws_region" {
  type        = string
  description = "This is the aws region to deploy things to."
  default     = "us-east-1"
}

variable "aws_access_key" {
  type        = string
  description = "This is the aws access key."
}

variable "aws_secret_key" {
  type        = string
  description = "This is the aws secret key."
}

//////////
// Compute machine variables
//////////

variable "master_ips" {
  type    = list(string)
  default = []
}

variable "worker_ips" {
  type = list(string)
  default = []
}

variable "aio_ips" {
  type = list(string)
  default = []
}

variable "compute_cpus" {
  type    = string
  default = "2"
}

variable "compute_memory" {
  type    = string
  default = "4096"
}

/////////
// Rancher variables
////////

variable "cloud_config" {
  type = string
  description = "Cloud config to use for images"
}

variable "ssh_keys" {
  type = list
  description = "List of SSH keys to add to template"
}

variable "bastion_host_ip" {
  type = string
  description = "Ip of the bastion host to use for cluster configuration"
}

variable "bastion_template" {
  type = string
  description = "Template of the bastion host"
}

variable "rancher_ssh_key" {
  type = string
  description = "SSH key for the rancher hosts"
}

variable "rancher_config_template" {
  type = "string"
  description = "Path to rancher node config template"
}