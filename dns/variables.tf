variable "cluster_domain" {
  description = "The domain for the cluster that all DNS records must belong"
  type        = string
}

variable "base_domain" {
  description = "The base domain used for public records."
  type        = string
}

variable "master_addresses" {
  type = list
}

variable "worker_addresses" {
  type = list
}

variable "aio_addresses" {
  type = list
}

variable "bastion_ip" {
  type = string
}