variable "name" {
  type = "string"
}

variable "cluster_domain" {
  type = "string"
}

variable "master_addresses" {
  type = "list"
}

variable "worker_addresses" {
  type = "list"
}

variable "aio_addresses" {
  type = "list"
}

variable "catalog" {
  type = "string"
}

variable "template" {
  type = "string"
}

variable "network" {
  type = "string"
}

variable "edge_gateway" {
  type = "string"
}

variable "edge_ip" {
  type = "string"
  description = "External IP of the edge"
}

variable "network_cidr" {
  type = "string"
  description = "CIDR of the network"
}

variable "network_gateway" {
  type = "string"
}

variable "cpus" {
  type    = "string"
  default = "2"
}

variable "memory" {
  type    = "string"
  default = "4096"
}

variable "cloud_config" {
  type = "string"
  description = "Template for your cloud config"
}

variable "ssh_keys" {
  type = "list"
  description = "List of ssh_keys to add access for"
}

variable "bastion_host_ip" {
  type = "string"
  description = "IP of the bastion host to use"
}

variable "bastion_template" {
  type = "string"
  description = "Template used to deploy your bastion host"
}

variable "external_network" {
  type = "string"
  description = "Name of external network attached to edge gateway"
}

variable "static_ip_start" {
  type = "string"
  description = "Start of the static ip pool range"
}

variable "static_ip_end" {
  type = "string"
  description = "End of the static ip pool range"
}

variable "rancher_ssh_key" {
  type = "string"
  description = "ssh_key to access nodes"
}