locals {
 master_nodes = var.master_addresses
 worker_nodes = var.worker_addresses
 aio_nodes = var.aio_addresses
 etcd_ports = {
    rancher_nodes = "2376"
    etcd_cp_nodes = "2379-2380"
    all_nodes = "8472"
    etcd = "9099"
    cp = "10250"
  }
  cp_ports = {
    http = "80"
    https = "443"
    all_nodes = "6443"
    cp = "10254"
    node_port = "3000-32767"
  }
  worker_ports = {
    ssh = "22"
    wmi = "3389"
  }
}


resource "vcd_network_routed" "net" {
  name = var.network
  edge_gateway = var.edge_gateway
  gateway = var.network_gateway

  static_ip_pool {
    start_address = "192.168.11.10"
    end_address = "192.168.11.40"
  }

  dhcp_pool {
    start_address = "192.168.11.140"
    end_address = "192.168.11.200"
  }
}

resource "vcd_firewall_rules" "fw" {
  edge_gateway = var.edge_gateway
  default_action = "allow"

  rule {
    description      = "Outbound internet"
    policy           = "allow"
    protocol         = "any"
    destination_port = "any"
    destination_ip   = "any"
    source_port      = "any"
    source_ip        = var.edge_ip
  }

  rule {
    description      = "SSH"
    policy           = "allow"
    protocol         = "tcp"
    destination_port = "22"
    destination_ip   = "any"
    source_port      = "22"
    source_ip        = "any"
  }

  dynamic rule {
    for_each = local.etcd_ports
    content {
      description      = "etcd ${rule.key}"
      policy           = "allow"
      protocol         = "tcp"
      destination_port = rule.value
      destination_ip   = "${var.network_gateway}/24"
      source_port      = rule.value
      source_ip        = "${var.network_gateway}/24"
    }
  }

  dynamic rule {
    for_each = local.cp_ports
    content {
      description      = "cp ${rule.key}"
      policy           = "allow"
      protocol         = "tcp"
      destination_port = rule.value
      destination_ip   = "${var.network_gateway}/24"
      source_port      = rule.value
      source_ip        = "${var.network_gateway}/24"
    }
  }

  dynamic rule {
    for_each = local.worker_ports
    content {
      description      = "worker ${rule.key}"
      policy           = "allow"
      protocol         = "tcp"
      destination_port = rule.value
      destination_ip   = "${var.network_gateway}/24"
      source_port      = rule.value
      source_ip        = "${var.network_gateway}/24"
    }
  }

  depends_on = ["vcd_network_routed.net"]
}

resource "vcd_snat" "outbound" {
  edge_gateway    = var.edge_gateway
  network_type    = "ext"
  network_name    = var.external_network
  description     = "Outbound internet access snat"
  external_ip     = var.network_cidr
  internal_ip     = var.edge_ip

  depends_on = ["vcd_firewall_rules.fw"]
}

resource "vcd_snat" "bastion" {
  edge_gateway = var.edge_gateway
  network_type = "ext"
  network_name = var.external_network
  description  = "Public to Edge access"
  external_ip  = "0.0.0.0/0" // CHANGE THIS TO BE MORE RESTRICTIVE
  internal_ip  = var.edge_ip

  depends_on = ["vcd_firewall_rules.fw"]
}

resource "vcd_dnat" "bastionssh" {
  edge_gateway    = var.edge_gateway
  network_type    = "ext"
  network_name    = var.external_network
  description     = "Bastion host ssh access"
  external_ip     = var.edge_ip
  internal_ip     = var.bastion_host_ip
  port            = 22
  translated_port = 22

  depends_on = ["vcd_firewall_rules.fw"]
}


resource "vcd_vapp" "bastion" {
  name             = "${var.name}-bastion"
  power_on         = false

  metadata = {
    role           = "bootstrap"
    cluster_domain = var.cluster_domain
  }

  depends_on = ["vcd_firewall_rules.fw"]
}

resource "vcd_vapp_vm" "bastion-vm" {
  vapp_name        = "${var.name}-bastion"
  name             = "${var.name}"
  catalog_name     = var.catalog
  template_name    = var.bastion_template
  cpus             = 2
  memory           = 4096
  cpu_cores        = 1

  network {
    type               = "org"
    name               = var.network
    ip_allocation_mode = "POOL"
  }

  metadata = {
    role           = "bastion"
    cluster_domain = var.cluster_domain
  }
  
  depends_on = ["vcd_vapp.bastion"]
}

resource "vcd_vapp" "cluster" {
  name             = "${var.name}"
  power_on         = false

  metadata = {
    role           = "${var.name}-cluster"
    cluster_domain = var.cluster_domain
  }

  depends_on = ["vcd_vapp_vm.bastion-vm"]
}

resource "vcd_vapp_vm" "master-vm" {
  count            = length(var.master_addresses)

  vapp_name        = "${var.name}"
  name             = "${var.name}-master-${count.index}"
  catalog_name     = var.catalog
  template_name    = var.template
  cpus             = var.cpus
  memory           = var.memory
  cpu_cores        = 1


  guest_properties = {
    "guestinfo.coreos.config.data" = base64encode(data.ignition_config.master_ign.*.rendered[count.index])
    "guestinfo.coreos.config.data.encoding" = "base64"
  }

  network {
    type               = "org"
    name               = var.network
    ip_allocation_mode = "MANUAL"
    ip                 = var.master_addresses[count.index]
    is_primary         = true
  }

  metadata = {
    role           = "${var.name}-master-node"
    cluster_domain = var.cluster_domain
  }
  
  depends_on = ["vcd_vapp.cluster"]
}

resource "vcd_vapp_vm" "worker-vm" {
  count            = length(var.worker_addresses)

  vapp_name        = "${var.name}"
  name             = "${var.name}-worker-${count.index}"
  catalog_name     = var.catalog
  template_name    = var.template
  cpus             = var.cpus
  memory           = var.memory
  cpu_cores        = 1


  guest_properties = {
    "guestinfo.coreos.config.data" = base64encode(data.ignition_config.worker_ign.*.rendered[count.index])
    "guestinfo.coreos.config.data.encoding" = "base64"
  }

  network {
    type               = "org"
    name               = var.network
    ip_allocation_mode = "MANUAL"
    ip                 = var.worker_addresses[count.index]
    is_primary         = true
  }

  metadata = {
    role           = "${var.name}-worker-node"
    cluster_domain = var.cluster_domain
  }
  
  depends_on = ["vcd_vapp.cluster"]
}

resource "vcd_vapp_vm" "aio-vm" {
  count            = length(var.aio_addresses)

  vapp_name        = "${var.name}"
  name             = "${var.name}-aio-${count.index}"
  catalog_name     = var.catalog
  template_name    = var.template
  cpus             = var.cpus
  memory           = var.memory
  cpu_cores        = 1


  guest_properties = {
    "guestinfo.coreos.config.data" = base64encode(data.ignition_config.aio_ign.*.rendered[count.index])
    "guestinfo.coreos.config.data.encoding" = "base64"
  }

  network {
    type               = "org"
    name               = var.network
    ip_allocation_mode = "MANUAL"
    ip                 = var.aio_addresses[count.index]
    is_primary         = true
  }

  metadata = {
    role           = "${var.name}-aio-node"
    cluster_domain = var.cluster_domain
  }
  
  depends_on = ["vcd_vapp.cluster"]
}