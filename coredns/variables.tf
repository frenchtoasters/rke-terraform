variable "access_key" {
  type        = string
  description = "This is the aws access key."
}

variable "secret_key" {
  type        = string
  description = "This is the aws secret key."
}

variable "edge_ip" {
  type = "string"
  description = "External IP of the edge"
}

variable "cluster_domain" {
  description = "The domain for the cluster that all DNS records must belong"
  type        = string
}

variable "zone_id" {
  type        = string
  description = "This is the zone id of the route53 zone created"
}

variable "bastion_host" {
  type    = string
}