locals {
  mask = "${element(split("/", var.network_cidr), 1)}"
  gw   = "${cidrhost(var.network_cidr,1)}"
}

data "ignition_user" "core" {
  name = "core"
  password_hash = "$1$aZYzlOe7$TT.1d8pb19RbhNcl9CKbp."
  ssh_authorized_keys = var.ssh_keys
  groups = ["docker","sudo"]
  shell = "/bin/bash"
}

data "ignition_file" "master_hostname" {
  count = length(var.master_addresses)

  filesystem = "root"
  path       = "/etc/hostname"
  mode       = "420"

  content {
    content = "${var.name}-master-${count.index}"
  }
}

data "ignition_file" "worker_hostname" {
  count = length(var.worker_addresses)

  filesystem = "root"
  path       = "/etc/hostname"
  mode       = "420"

  content {
    content = "${var.name}-worker-${count.index}"
  }
}

data "ignition_file" "aio_hostname" {
  count = length(var.aio_addresses)

  filesystem = "root"
  path       = "/etc/hostname"
  mode       = "420"

  content {
    content = "${var.name}-aio-${count.index}"
  }
}

data "ignition_file" "master_static_ip" {
  count = length(var.master_addresses)

  filesystem = "root"
  path       = "/etc/sysconfig/network-scripts/ifcfg-ens192"
  mode       = "420"

  content {
    content = <<EOF
TYPE=Ethernet
BOOTPROTO=none
NAME=ens192
DEVICE=ens192
ONBOOT=yes
IPADDR=${var.master_addresses[count.index]}
PREFIX=${local.mask}
GATEWAY=${local.gw}
DOMAIN=${var.cluster_domain}
DNS1=1.1.1.1
DNS2=9.9.9.9
EOF
  }
}

data "ignition_file" "worker_static_ip" {
  count = length(var.worker_addresses)

  filesystem = "root"
  path       = "/etc/sysconfig/network-scripts/ifcfg-ens192"
  mode       = "420"

  content {
    content = <<EOF
TYPE=Ethernet
BOOTPROTO=none
NAME=ens192
DEVICE=ens192
ONBOOT=yes
IPADDR=${var.worker_addresses[count.index]}
PREFIX=${local.mask}
GATEWAY=${local.gw}
DOMAIN=${var.cluster_domain}
DNS1=1.1.1.1
DNS2=9.9.9.9
EOF
  }
}

data "ignition_networkd_unit" "aio_static_ip" {
  count = length(var.aio_addresses)

  name = "00-ens192.network"
  content = <<EOF
    [Match]
    Name=ens192

    [Network]
    Address=${var.aio_addresses[count.index]}/24
    Gateway=${local.gw}
    DNS=8.8.8.8
  EOF
}

data "ignition_config" "master_ign" {
  count = length(var.master_addresses)

  users = [
    "${data.ignition_user.core.id}",
  ]

  files = [
    "${data.ignition_file.master_hostname.*.id[count.index]}",
    "${data.ignition_file.master_static_ip.*.id[count.index]}",
  ]
}

data "ignition_config" "worker_ign" {
  count = length(var.worker_addresses)

  users = [
    "${data.ignition_user.core.id}",
  ]

  files = [
    "${data.ignition_file.worker_hostname.*.id[count.index]}",
    "${data.ignition_file.worker_static_ip.*.id[count.index]}",
  ]
}

data "ignition_config" "aio_ign" {
  count = length(var.aio_addresses)

  users = [
    "${data.ignition_user.core.id}",
  ]

  networkd = [
    "${data.ignition_networkd_unit.aio_static_ip.*.id[count.index]}",
  ]

  files = [
    "${data.ignition_file.aio_hostname.*.id[count.index]}",
  ]
}