provider "vcd" {
  url                  = var.vcd_url
  user                 = var.vcd_user
  password             = var.vcd_password

  sysorg               = "System"
  org                  = var.vcd_org
  vdc                  = var.vcd_vdc
  allow_unverified_ssl = true
}

provider "aws" {
  region     = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

module "vcd_nodes" {
  source                  = "./vcd"

  name                    = "Rancher"

  bastion_host_ip         = var.bastion_host_ip
  bastion_template        = var.bastion_template

  cluster_domain          = var.cluster_domain
  catalog                 = var.catalog_name
  template                = var.template_name
  network                 = var.vapp_network
  master_addresses        = var.master_ips
  worker_addresses        = var.worker_ips
  aio_addresses           = var.aio_ips
  cloud_config            = var.cloud_config
  ssh_keys                = var.ssh_keys

  edge_ip                 = var.vcd_edge_ip
  external_network        = var.vcd_external_network
  edge_gateway            = var.vcd_edge
  network_gateway         = var.vcd_network_gateway
  network_cidr            = var.vcd_network_cidr
  static_ip_start         = var.vcd_static_ip_start 
  static_ip_end           = var.vcd_static_ip_end

  cpus                    = var.compute_cpus
  memory                  = var.compute_memory

  rancher_ssh_key         = var.rancher_ssh_key
}

module "rke" {
  source                  = "./rke"

  master_addresses        = module.vcd_nodes.master_nodes
  worker_addresses        = module.vcd_nodes.worker_nodes
  aio_addresses           = module.vcd_nodes.aio_nodes
  
  edge_ip                 = var.vcd_edge_ip
  rancher_ssh_key         = var.rancher_ssh_key
  rancher_config_template = var.rancher_config_template
  cluster_domain          = var.cluster_domain
}

/*
module "dns" {
  source              = "./dns"

  base_domain         = var.base_domain
  cluster_domain      = var.cluster_domain
  master_addresses    = var.master_ips
  worker_addresses    = var.worker_ips
  aio_addresses       = var.aio_ips

  bastion_ip          = var.bastion_host_ip
}

module "coredns" {
  source         = "./coredns"

  bastion_host   = module.vcd_nodes.bastion_host
  access_key     = var.aws_access_key
  secret_key     = var.aws_secret_key

  zone_id        = module.dns.dns_zone
  edge_ip        = var.vcd_edge_ip
  cluster_domain = var.cluster_domain
}
*/